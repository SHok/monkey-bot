#!/usr/bin/perl -w

system('clear');

use strict;
use Net::XMPP;
use MIME::Base64;
use File::Slurp;
use Data::Dumper;
use Config::File 'read_config_file';

my $settings = read_config_file('config.ini');
our %plugins = ();
load_plugins();

# Если не удалось прочитать настройки - завершаем работу
unless ( $settings || $settings->{'NET'}{'server'} || $settings->{'NET'}{'login'} || $settings->{'NET'}{'password'} || $settings->{'NET'}{'admins'} ) {
    die("Can't read settings!");
}

# Массив, в котором будут JID админов
my @admins = split(',', $settings->{'NET'}{'admins'});

my $client = Net::XMPP::Client->new();

$client->SetCallBacks(onauth => \&onAuth);
$client->SetMessageCallBacks(
	chat    =>\&messageChatCB,
	);

$client->Execute(
	hostname => $settings->{'NET'}{'server'},
	username => $settings->{'NET'}{'login'},
	password => $settings->{'NET'}{'password'},
	resource => $settings->{'NET'}{'resource'},
	);

sub onAuth {
	$client->PresenceSend(
		show => 'Online',
		priority => 10
		);
	
	my ($second, $minute, $hour) = (localtime(time()))[0..2];
	for (@admins) {
	print "[".$hour.":".$minute.":".$second."] Admin info send [".$_."]\n";
	$client->MessageSend(
		to   => $_,
		body => "\n[".$hour.":".$minute.":".$second."]\n".'Monky bot started!',
		type => 'chat'
		);
	}
}

sub messageChatCB {
	my ($sid, $mes) = @_;
	my $sender = $mes->GetFrom();
	my $body   = $mes->GetBody();
	my $response = undef;
	my ($second, $minute, $hour) = (localtime(time()))[0..2];
	print "[".$hour.":".$minute.":".$second."] [".$mes->GetType."] [".$sender."] => [".$body."]\n";
	
	my ($comand, $parametrs) = $body =~ m#^(.+?)\s+(.+)$#;
	unless ($comand and $parametrs) {
		$comand = $body;
	} else {
		$parametrs = encode_base64($parametrs);
	}
	
	if ($plugins{$comand}) {
		if ($parametrs) { 
			eval('$response = &'.$plugins{$comand}{'space'}.'::comand(decode_base64(\''.$parametrs.'\'), $mes, $sid, $client)');
		} else {
			eval('$response = &'.$plugins{$comand}{'space'}.'::comand(\'\', $mes, $sid, $client)');
		}
	} else {
		my ($second, $minute, $hour) = (localtime(time()))[0..2];
		print "[".$hour.":".$minute.":".$second."] [SEND] [".$mes->GetType."] [".$sender."] => [Not found comand]\n";
		$client->MessageSend(
			to   => $sender,
			body => "[".$hour.":".$minute.":".$second."]\n".'Not found command!',
			type => $mes->GetType,
			);
		return;
	}
	
	if ($response) {
		my ($second, $minute, $hour) = (localtime(time()))[0..2];
		print "[".$hour.":".$minute.":".$second."] [SEND] [".$mes->GetType."] [".$sender."] => [".$response."]\n";
		$client->MessageSend(
			to   => $sender,
			body => "[".$hour.":".$minute.":".$second."]\n".$response,
			type => $mes->GetType,
			);
	} else {
		my ($second, $minute, $hour) = (localtime(time()))[0..2];
		print "[".$hour.":".$minute.":".$second."] [SEND] [".$mes->GetType."] [".$sender."] => [".$response."]\n";
		$client->MessageSend(
			to   => $sender,
			body => "[".$hour.":".$minute.":".$second."]\n".'Comand return NULL.',
			type => $mes->GetType,
			);
	}
}

sub load_plugins {
	for (glob('plugins/*.pm')) {
		require($_);
		my ($space) = read_file($_) =~ m#package (\w+);#;
		my %info = ();
		eval('%info = &'.$space.'::start()');
		$info{'space'} = $space;
		$plugins{$info{'comand'}} = \%info;
	}
}
