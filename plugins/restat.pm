package restart;
use Data::Dumper;

my %info = (
	'comand'  => 'restart',
	'version' => '1.0',
	'author'  => 'SHok',
);

sub start {
	return %info;
}

sub comand {
	my ($param, $msg, $sid, $client) = @_;
	$client->MessageSend(
		to   => $msg->GetFrom(),
		body => 'Go to restart ...',
		type => 'chat'
		);
	$client->Disconnect();
	exec($0);
}

1;
