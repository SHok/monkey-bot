package test;
use Data::Dumper;

my %info = (
	'comand'  => 'test',
	'version' => '1.0',
	'author'  => 'SHok',
);

sub start {
	return %info;
}

sub comand {
	my ($param) = @_;
	if ($param) {
		return 'Params :'."\n".$param."\nDump:\n".Dumper(\%info)."\n";
	} else {
		return "Dump:\n".Dumper(\%info)."\n";
	}
}

1;
