package cd;
use Data::Dumper;
use Cwd;

my %info = (
	'comand'  => 'cd',
	'version' => '1.0',
	'author'  => 'SHok',
);

sub start {
	return %info;
}

sub comand {
	my ($param) = @_;
	chdir($param);
	return 'OK : '.getcwd();
}

1;
