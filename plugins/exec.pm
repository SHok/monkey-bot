package exe;
use Data::Dumper;

my %info = (
	'comand'  => 'exec',
	'version' => '1.0',
	'author'  => 'SHok',
);

sub start {
	return %info;
}

sub comand {
	my ($param, $msg, $sid, $client) = @_;
	return `$param`;
}

1;
