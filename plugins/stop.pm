package stop;
use Data::Dumper;

my %info = (
	'comand'  => 'stop',
	'version' => '1.0',
	'author'  => 'SHok',
);

sub start {
	return %info;
}

sub comand {
	my ($param, $msg, $sid, $client) = @_;
	$client->MessageSend(
		to   => $msg->GetFrom(),
		body => 'Shat down ...',
		type => 'chat'
		);
	$client->Disconnect();
	exit;
}

1;
